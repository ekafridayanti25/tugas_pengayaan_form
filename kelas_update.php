<?php
$id=$_GET['id'];

$sql = "SELECT*FROM kelas WHERE id_kelas='$id'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$nmkelase=$row['nama_kelas'];

if(isset($_POST['update'])){
    $nama=$_POST['nama'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];

    if($nmkelase!=$nama){
        $sql = "SELECT*FROM kelas WHERE nama_kelas='$nama'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            ?>
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>
                    Nama Kelas sudah digunakan
                </div>
                </div>  
            <?php
        }else{
            goto simpan;
        }
    }else{
        simpan:
        $sql = "UPDATE kelas SET nama_kelas='$nama',prodi='$prodi',fakultas='$fakultas' WHERE id_kelas='$id'";
        if ($conn->query($sql) === TRUE) {
            header("location:index.php?page=kelas");
        }
    }
    
}

?>

<div class="row">
    <div class="col-lg-8 offset-lg-2">
        <div class="card">
            <div class="card-header border text-dark"><b>Update Data Kelas</b></div>
                <div class="card-body border">
                    <form action="" name="Form" method="POST">
                        <div class="form-group">
                            <label for="">Nama kelas : </label>
                            <input type="text" class="form-control mb-2" name="nama" value="<?php echo $row['nama_kelas']; ?>" maxlength="100" placeholder="Nama Dosen" required>
                        </div>
                        <div class="form-group">
                            <label for="">Prodi : </label>
                            <input type="text" class="form-control mb-2" name="prodi" value="<?php echo $row['prodi']; ?>" maxlength="100" placeholder="Prodi" required>
                        </div>
                        <div class="form-group">
                            <label for="">Fakultas : </label>
                            <input type="text" class="form-control mb-2" name="fakultas" value="<?php echo $row['fakultas']; ?>" maxlength="100" placeholder="Fakultas" required>
                        </div>
                        <div class="mt-3">
                            <input class="btn btn-primary" type="submit" name="update" value="Update">
                            <a class="btn btn-danger" href="?page=kelas">Batal</a>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

<?php 
    $conn->close();
?>
