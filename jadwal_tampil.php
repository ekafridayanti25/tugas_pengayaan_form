<div class="card">
  <div class="card-header text-dark"><b>Data jadwal</b></div>
  <div class="card-body border">
  <table class="table table-bordered" id="myTable">
    <a class="btn btn-primary" href="?page=jadwal&action=tambah" style="margin-bottom:10px;">
        <span class="fa fa-plus"></span> Tambah
    </a>
    <thead class="thead-light">
      <tr>
        <th width="50px">No</th>
        <th width="200px">Nama Dosen</th>
        <th width="200px">Nama Kelas</th>
        <th width="80px">Jadwal</th>
        <th width="200px">Makul</th>
        <th width="80px"></th>
      </tr>
    </thead>
    <tbody>
        <?php
            $i=1;
            $sql = "SELECT * FROM vjadwal ORDER BY id_jadwal ASC";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $row['nama_dosen']; ?></td>
                <td><?php echo $row['nama_kelas']; ?></td>
                <td><?php echo $row['jadwal']; ?></td>
                <td><?php echo $row['mata_kuliah']; ?></td>
                <td align="center">
                    <a class="btn btn-warning" href="?page=jadwal&action=update&id=<?php echo $row['id_jadwal']; ?>">
                        <span class="fa fa-wrench"></span>
                    </a>
                    <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=jadwal&action=hapus&id=<?php echo $row['id_jadwal']; ?>">
                        <span class="fa fa-trash"></span>
                    </a>
                </td>
            </tr>
        <?php }
            $conn->close();
        ?>
    </tbody>
  </table>
  </div>
</div>