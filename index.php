<?php
require "config.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/datatables.bootstrap5.min.css">
    <link rel="stylesheet" href="css/all-min.css">
    <link rel="stylesheet" href="css/bootstrap-chosen.css">

    <title>SIM_JADWAL</title>
  </head> 

   <!-- awal navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">
    SISTEM INFORMASI JADWAL DOSEN
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
      <li class="nav-item">
          <a class="nav-link active" href="index.php">HOME</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="?page=dosen">DOSEN</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="?page=kelas">KELAS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="?page=jadwal">JADWAL</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- akhhir navbar -->

<!-- awal container -->
<div class="container mt-xxl-5" style="margin-top:80px">
    <?php
        $page = isset($_GET['page']) ? $_GET['page'] : "";
        $action = isset($_GET['action']) ? $_GET['action'] : "";

        if ($page==""){
            include "welcome.php";
        }elseif ($page=="dosen"){
          if ($action==""){
              include "dosen_tampil.php";
          }elseif ($action=="tambah"){
              include "dosen_tambah.php";
          }elseif ($action=="update"){
              include "dosen_update.php";
          }else{
              include "dosen_hapus.php";
          }
        }elseif ($page=="kelas"){
          if ($action==""){
            include "kelas_tampil.php";
          }elseif ($action=="tambah"){
              include "kelas_tambah.php";
          }elseif ($action=="update"){
              include "kelas_update.php";
          }elseif ($action=="hapus"){
              include "kelas_hapus.php";
          }
        }elseif ($page=="jadwal"){
          if ($action==""){
            include "jadwal_tampil.php";
          }elseif ($action=="tambah"){
              include "jadwal_tambah.php";
          }elseif ($action=="update"){
              include "jadwal_update.php";
          }elseif ($action=="hapus"){
              include "jadwal_hapus.php";
          }
        }else{
          include "logout.php";
        }
    ?>
</div>

<!-- akhir container -->

    <!-- Optional JavaScript; choose one of the two! -->
    <script src="js/popper.min.js"></script>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/jquery.datatables.min.js"></script>
    <script src="js/datatables.bootstrap5.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/chosen.jquery.min.js"></script>

    <script>
       $(document).ready(function () {
           $('#myTable').dataTable();
       });
    </script>

    <script>
     $(function() {
       $('.chosen').chosen();
     });
    </script>

  </body>
</html>