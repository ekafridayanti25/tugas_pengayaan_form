<?php

if(isset($_POST['simpan'])){
   
    $tgl=$_POST['tgl'];;
    $iddosen=$_POST['nmdosen'];
    $idkelas=$_POST['nmkelas'];
    $makul=$_POST['makul'];

    $sql = "SELECT*FROM jadwal_kelas WHERE id_dosen='$iddosen' AND id_kelas='$idkelas' AND jadwal='$tgl'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        ?>
            <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                <div>Jadwal sudah dimasukkan</div>
            </div>      
        <?php
    }else{
        $sql = "INSERT INTO jadwal_kelas VALUES (Null,'$iddosen','$idkelas','$tgl','$makul')";
        if ($conn->query($sql) === TRUE) {
            $last_id = $conn->insert_id;
            header("Location:?page=jadwal");
        }
    }
}
?>

<div class="row">
    <div class="col-lg-6 offset-lg-3">
        <div class="card">
            <div class="card-header border text-dark"><b>Input Data Jadwal</b></div>
                <div class="card-body border">
                    <form action="" method="POST">
                  
                        <div class="form-group mb-2">
                            <label for="">Nama Dosen : </label>
                            <select class="form-control chosen" data-placeholder="Pilih Nama Dosen" name="nmdosen">
                                <option value=""> </option>;
                                <?php
                                    $sql2 = "SELECT * FROM dosen order by nama_dosen asc";
                                    $result2 = $conn->query($sql2);
                                    while($row2 = $result2->fetch_assoc()) {
                                ?>
                                    <option value="<?php echo $row2['id_dosen'] ?>"><?php echo $row2['nama_dosen'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>               
                        </div>
                        <div class="form-group mb-2">
                            <label for="">Nama Kelas : </label>
                            <select class="form-control chosen" data-placeholder="Pilih Nama Kelas" name="nmkelas">
                                <option value=""> </option>;
                                <?php
                                    $sql2 = "SELECT * FROM kelas order by nama_kelas asc";
                                    $result2 = $conn->query($sql2);
                                    while($row2 = $result2->fetch_assoc()) {
                                ?>
                                    <option value="<?php echo $row2['id_kelas'] ?>"><?php echo $row2['nama_kelas'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>               
                        </div>
                        <div class="form-group">
                            <label for="">Jadwal (m-d-Y) : </label>
                            <input type="date" class="form-control mb-2" name="tgl" required>
                        </div> 
                        <div class="form-group mt-2">
                            <label for="">Nama Mata Kuliah : </label>
                            <input type="text" class="form-control mb-2" name="makul" maxlength="100" placeholder="Nama Mata Kuliah" required>
                        </div>
                        <div class="mt-3">
                            <input class="btn btn-primary" type="submit" name="simpan" value="Simpan">
                            <a class="btn btn-danger" href="?page=jadwal">Batal</a>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

<?php 
    $conn->close();
?>