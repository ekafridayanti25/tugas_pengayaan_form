<div class="card">
  <div class="card-header text-dark"><b>Data Dosen</b></div>
  <div class="card-body border">
  <table class="table table-bordered" id="myTable">
    <a class="btn btn-primary" href="?page=dosen&action=tambah" style="margin-bottom:10px;">
     <span class="fa fa-plus"></span> Tambah
    </a>
    <thead class="thead-light">
      <tr>
        <th width="100px">NIP</th>
        <th width="200px">Nama Dosen</th>
        <th width="100px">Prodi</th>
        <th width="100px">Fakultas</th>
        <th width="100px">Gambar</th>
        <th width="80px">Aksi</th>
      </tr>
    </thead>
    <tbody>
        <?php
            $sql = "SELECT * FROM dosen ORDER BY nip_dosen ASC";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) {
        ?>
            <tr>
                <?php $gambare=base64_encode($row['foto_dosen']); ?>
                <td><?php echo $row['nip_dosen']; ?></td>
                <td><?php echo $row['nama_dosen']; ?></td>
                <td><?php echo $row['prodi']; ?></td>
                <td><?php echo $row['fakultas']; ?></td>
                <td><img class="img-thumbnail" id="preview" src="data:image/jpg;base64, <?php echo $gambare; ?>" width="100px" height="90px"/></td>
                <td align="center">
                    <a class="btn btn-warning" href="?page=dosen&action=update&id=<?php echo $row['id_dosen']; ?>">
                        <span class="fa fa-wrench"></span>
                    </a>
                    <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=dosen&action=hapus&id=<?php echo $row['id_dosen']; ?>">
                        <span class="fa fa-trash"></span>
                    </a>
                </td>
            </tr>
        <?php }
            $conn->close();
        ?>
    </tbody>
  </table>
  </div>
</div>