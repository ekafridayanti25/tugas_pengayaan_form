<?php

if(isset($_POST['simpan'])){

    $id=$_POST['id'];
    $nama=$_POST['nama'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];

    $sql = "SELECT*FROM kelas WHERE nama_kelas='$nama'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        ?>
        <script type="text/javascript">
        alert("Nama Kelas sudah digunakan");
        window.location.href="?page=kelas&action=tambah";
        </script>
        <?php
    }else{
        $sql = "INSERT INTO kelas VALUES (Null,'$nama','$prodi','$fakultas')";
        if ($conn->query($sql) === TRUE) {
            header("location:index.php?page=kelas");
        }
    }
  }
$conn->close();
?>

<div class="row">
    <div class="col-lg-8 offset-lg-2">
        <div class="card">
            <div class="card-header border text-dark"><b>Input Data Kelas</b></div>
                <div class="card-body border">
                    <form action="" name="Form" method="POST">
                        <div class="form-group">
                            <label for="">Nama Kelas : </label>
                            <input type="text" class="form-control mb-2" name="nama" maxlength="100" placeholder="Nama Kelas" required>
                        </div>
                        <div class="form-group">
                            <label for="">Prodi : </label>
                            <input type="text" class="form-control mb-2" name="prodi" maxlength="100" placeholder="Prodi" required>
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Fakultas : </label>
                            <input type="text" class="form-control mb-2" name="fakultas" maxlength="100" placeholder="Fakultas" required>
                        </div>
                        <div class="mt-3">
                            <input class="btn btn-primary" type="submit" name="simpan" value="Simpan">
                            <a class="btn btn-danger" href="?page=kelas">Batal</a>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
