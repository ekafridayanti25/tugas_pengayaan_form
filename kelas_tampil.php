<div class="card">
  <div class="card-header text-dark"><b>Data Kelas</b></div>
  <div class="card-body border">
  <table class="table table-bordered" id="myTable">
    <a class="btn btn-primary" href="?page=kelas&action=tambah" style="margin-bottom:10px;">
     <span class="fa fa-plus"></span> Tambah
    </a>
    <thead class="thead-light">
      <tr>
        <th width="50px">No</th>
        <th width="400px">Nama Kelas</th>
        <th width="200px">Prodi</th>
        <th width="200px">Fakultas</th>
        <th width="80px">Aksi</th>
      </tr>
    </thead>
    <tbody>
        <?php
            $i=1;
            $sql = "SELECT * FROM kelas ORDER BY id_kelas ASC";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) {
        ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $row['nama_kelas']; ?></td>
                <td><?php echo $row['prodi']; ?></td>
                <td><?php echo $row['fakultas']; ?></td>
                <td align="center">
                    <a class="btn btn-warning" href="?page=kelas&action=update&id=<?php echo $row['id_kelas']; ?>">
                        <span class="fa fa-wrench"></span>
                    </a>
                    <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=kelas&action=hapus&id=<?php echo $row['id_kelas']; ?>">
                        <span class="fa fa-trash"></span>
                    </a>
                </td>
            </tr>
        <?php }
            $conn->close();
        ?>
    </tbody>
  </table>
  </div>
</div>