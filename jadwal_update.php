<?php

$id=$_GET['id'];

$sql = "SELECT*FROM vjadwal WHERE id_jadwal='$id'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$iddosene=$row['id_dosen'];
$idkelase=$row['id_kelas'];
$tgle=$row['jadwal'];

if(isset($_POST['update'])){
    
    $tgl=$_POST['tgl'];
    $makul=$_POST['makul'];

    if($tgle!=$tgl){
        $sql = "SELECT*FROM jadwal_kelas WHERE id_dosen='$iddosene' AND id_kelas='$idkelase' AND jadwal='$tgl'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            ?>
                <div class="alert alert-danger d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
                    <div>Jadwal sudah dimasukkan</div>
                </div>     
            <?php
        }else{
            goto simpan;
        }
    }else{
        simpan:
        $sql = "UPDATE jadwal_kelas SET jadwal='$tgl', mata_kuliah='$makul' WHERE id_jadwal='$id'";
        if ($conn->query($sql) === TRUE) {
            $last_id = $conn->insert_id;
            header("Location:?page=jadwal");
        }
    }
}

?>

<div class="row">
    <div class="col-lg-6 offset-lg-3">
        <div class="card">
            <div class="card-header border text-dark"><b>Update Data Jadwal</b></div>
                <div class="card-body border">
                    <form action="" method="POST">
                  
                        <div class="form-group mb-2">
                            <label for="">Nama Dosen : </label>
                            <input type="text" class="form-control mb-2" name="nmdosen" value="<?php echo $row['nama_dosen'] ?>" readonly>
                        </div>
                        <div class="form-group mb-2">
                            <label for="">Nama Kelas : </label>
                            <input type="text" class="form-control mb-2" name="nmkelas" value="<?php echo $row['nama_kelas'] ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Jadwal (m-d-Y) : </label>
                            <input type="date" class="form-control mb-2" name="tgl" value="<?php echo date('Y-m-d'); ?>" required>
                        </div> 
                        <div class="form-group mt-2">
                            <label for="">Nama Mata Kuliah : </label>
                            <input type="text" class="form-control mb-2" name="makul" value="<?php echo $row['mata_kuliah'] ?>" maxlength="100" placeholder="Nama Mata Kuliah" required>
                        </div>
                        <div class="mt-3">
                            <input class="btn btn-primary" type="submit" name="update" value="Update">
                            <a class="btn btn-danger" href="?page=jadwal">Batal</a>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

<?php 
$conn->close();
?>